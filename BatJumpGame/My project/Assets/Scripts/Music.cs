﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    [SerializeField] private AudioClip _audioClip = null;
    private void Start()
    {
        gameObject.GetComponent<AudioSource>().clip = _audioClip;
        gameObject.GetComponent<AudioSource>().Play();
    }
}
