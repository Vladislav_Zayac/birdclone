﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundMenu : MonoBehaviour
{
    [SerializeField] private Sprite _onSound = null;
    [SerializeField] private Sprite _offSound = null;
    [SerializeField] private GameObject _audioSource = null;

    private bool OnPlayMusic = true;

    public void Click()
    {
        if (OnPlayMusic)
        {
            _audioSource.GetComponent<AudioSource>().volume = 0;
            gameObject.GetComponent<Image>().sprite = _offSound;
            OnPlayMusic = false;
        }
        else
        {
            _audioSource.GetComponent<AudioSource>().volume = 1;
            gameObject.GetComponent<Image>().sprite = _onSound;
            OnPlayMusic = true;
        }
    }
}
