﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScale : MonoBehaviour
{
    [SerializeField] private Vector2 _screenResolution;

    private void Awake()
    {
        Camera.main.orthographicSize *= (_screenResolution.x / _screenResolution.y) / Camera.main.aspect;
    }

}
