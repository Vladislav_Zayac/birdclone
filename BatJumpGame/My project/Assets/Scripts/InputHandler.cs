﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public System.Action JumpAction=null;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            JumpAction?.Invoke();
        }
        if(Input.touchCount>0)
            if(Input.GetTouch(0).phase==TouchPhase.Began)
            {
                //Handheld.Vibrate();
                JumpAction?.Invoke();
            }
    }
}
