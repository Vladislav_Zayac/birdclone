﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorController : MonoBehaviour
{
    [SerializeField] private GameObject _floor = null;
    [SerializeField] private GameObject _robot = null;
    [SerializeField] private Player _player = null;

    private Queue<GameObject> _floors = new Queue<GameObject>(5);
    private Queue<GameObject> _bots = new Queue<GameObject>(5);
    private float _YPosition = -2.2f;

    public System.Action OnLose;

    private void OnEnable()
    {
        for (int i = 0; i < 5; i++)
        {
            GameObject floor = GameObject.Instantiate(_floor, new Vector3(0, _YPosition, 0), Quaternion.identity, gameObject.transform);
            _floors.Enqueue(floor);
            _YPosition += 2.5f;
        }
        _player.gameObject.SetActive(true);
        _player.OnCollisionAction += CollisionBots;
    }

    private void Update()
    {
        if (_bots?.Count != 0)
        {
            if (_player.transform.position.y - _bots?.Peek().transform.position.y > 5f)
            {
                GameObject bot = _bots.Dequeue();
                Destroy(bot);
            }
        }
        if (_player.transform.position.y - _floors?.Peek().transform.position.y > 5f)
        {
            GameObject floor = _floors.Dequeue();
            floor.transform.position = new Vector3(0, floor.transform.position.y + 12.5f, 0);
            int _typeBot = Random.Range(0, 10);
            if (_typeBot >= 1 && _typeBot <= 3)
            {
                var robot = GameObject.Instantiate(_robot, new Vector3(0, floor.transform.position.y + 0.7f, 0), Quaternion.identity);
                float _index = Random.Range(-2f, 2f);
                robot.transform.position = new Vector3(_index, robot.transform.position.y, 0);
                _bots.Enqueue(robot);
            }
            _floors.Enqueue(floor);
        }
    }

    private void CollisionBots()
    {
        while (_bots.Count != 0)
        {
            GameObject bot = _bots.Dequeue();
            Destroy(bot);
        }
        while (_floors.Count != 0)
        {
            GameObject floor = _floors.Dequeue();
            Destroy(floor);
        }
        gameObject.SetActive(false);
        OnLose?.Invoke();
    }

    private void OnDisable()
    {
        _floors = new Queue<GameObject>(5);
        _bots = new Queue<GameObject>(5);
        _YPosition = -2.2f;
        _player.OnCollisionAction -= CollisionBots;
        _player.gameObject.SetActive(false);
    }
}
