﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private GameObject _camera = null;
    [SerializeField] private Text _scoreText = null;
    [SerializeField] private float _speedMove=0f;
    [SerializeField] private float _jumpForce = 0f;
    [SerializeField] private InputHandler _inputHandler = null;
    public System.Action OnCollisionAction;
    private Vector2 _direction = Vector2.right;
    private float _epsilon = -0.1f;
    private float _cameraYPosition=0f;
    private float _tempY;
    private Vector3 _startPositionPlayer;

    public void ScoreDefault()
    {
        _tempY = 0f;
    }

    public float GetScore()
    {
        return _tempY;
    }

    private void Start()
    {
        _cameraYPosition = _camera.transform.position.y;
        _inputHandler.JumpAction += Jump;
        _tempY = transform.position.y;
        _startPositionPlayer = transform.position;
    }
    private void Update()
    {
        if (transform.position.x - 2.15f> _epsilon)
        {
            _direction = Vector2.left;
        }
        else if(transform.position.x + 2.15f< _epsilon)
        {
            _direction = Vector2.right;
        }
        transform.Translate(_speedMove * Time.deltaTime * _direction);
        OnCameraMove();
        if (_tempY < transform.position.y)
            _tempY = transform.position.y;
        _scoreText.text = Convert.ToString((int)_tempY * 10);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Bot")
        {
            OnCollisionAction?.Invoke();
        }
    }
    private void OnCameraMove()
    {
        _camera.transform.position = new Vector3(0,transform.position.y+_cameraYPosition+2,-10);
    }
    private void Jump()
    {
        gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * _jumpForce,ForceMode2D.Impulse);
    }
    private void OnEnable()
    {
        transform.position = _startPositionPlayer;
    }
    private void OnDisable()
    {
        ScoreDefault();
    }
}
