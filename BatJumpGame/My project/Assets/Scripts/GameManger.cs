﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManger : MonoBehaviour
{
    [SerializeField] private Player _player = null;
    [SerializeField] private Color _color5;
    [SerializeField] private Color _color4;
    [SerializeField] private Color _color3;
    [SerializeField] private Color _color2;
    [SerializeField] private Color _color1;
    [SerializeField] private Color _color0;

    private int count = -1;
    private float temp = 0f;
    private bool OnGo = true;

    private void Update()
    {
        if (OnGo)
        {
            if (count == 5)
                Camera.main.backgroundColor = _color5;
            else if (count == 4)
                Camera.main.backgroundColor = _color4;
            else if (count == 3)
                Camera.main.backgroundColor = _color3;
            else if (count == 2)
                Camera.main.backgroundColor = _color2;
            else if (count == 1)
                Camera.main.backgroundColor = _color1;
            else if (count == 0)
                Camera.main.backgroundColor = _color0;
            OnGo = false;
        }
        if (_player?.GetScore() > temp)
        {
            temp += 100f;
            count++;
            OnGo = true;
        }
    }
}
