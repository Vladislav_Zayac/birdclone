﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robot : MonoBehaviour
{
    [SerializeField] private float _speedMove = 0f;
    private Vector2 _direction = Vector2.right;
    private bool OnMove = false;
    private float _epsilon = -0.1f;
    private int _index = 0;
    private void Start()
    {
        _index = Random.Range(0, 3);
        if (_index == 1)
            OnMove = true;
    }
    private void Update()
    {
        if(OnMove)
            Move();
    }
    private void Move()
    {
        if (transform.position.x - 2.15f > _epsilon)
        {
            _direction = Vector2.left;
        }
        else if (transform.position.x + 2.15f < _epsilon)
        {
            _direction = Vector2.right;
        }
        transform.Translate(_speedMove * Time.deltaTime * _direction);
    }
}
