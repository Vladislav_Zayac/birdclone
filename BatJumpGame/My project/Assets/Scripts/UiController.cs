﻿using UnityEngine;

public class UiController : MonoBehaviour
{
    [Header("Screen")]
    [SerializeField] private GameObject _startScreen = null;
    [SerializeField] private GameObject _loseScreen = null;
    [SerializeField] private GameObject _gameScreen = null;

    [Header("Controller")]
    [SerializeField] private FloorController _controller = null;

    public void StartGame()
    {
        _startScreen.SetActive(false);
        _gameScreen.SetActive(true);
        _controller.gameObject.SetActive(true);
        _controller.OnLose += LoseGame;
    }

    public void RestartGame()
    {
        _loseScreen.SetActive(false);
        _controller.gameObject.SetActive(true);
    }

    public void LoseGame()
    {
        _loseScreen.SetActive(true);
    }
}
